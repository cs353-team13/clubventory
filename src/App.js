//import logo from './logo.svg';
import './App.css';
import React, { useEffect } from "react";
import SearchIcon from "@material-ui/icons/Search";
import ShoppingBasketIcon from "@material-ui/icons/ShoppingBasket";
import Header from './Header';
import Home from './Home';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from './Login';
import { auth } from "./firebase";

function App() {
  

  useEffect(() => { // sits here and listen
    // will only run once when the app component loads

    auth.onAuthStateChanged((authUser) => { 
      // as soon as app loads, it will always run this listener
      // it will give user
      console.log("THE USER IS >>> ", authUser);

      if (authUser) {
        // the user just logged in OR the user was logged in
        // if you refresh the page, it will still log you in
        
        /*
        dispatch({ 
          type: "SET_USER",
          user: authUser,
        });

        
        if(authUser != null){
          document.getElementById("user__log").innerHTML = "hello " + authUser.email;
        }

        */


      } 
      
      else {
        // the user is logged out
        /*dispatch({
          type: "SET_USER", // removes user from the data layer
          user: null, // user is now empty
        });
        */
        //document.getElementById("user__log").innerHTML = "Hello there XD why dont you login?";
      }
    });
  }, []); 

  

  return (
    // BEM convension

    // default route (home) is always at the bottom
    <Router>

      <div className="App">

          <Switch>

            <Route path = "/login">
              <Login/>
            </Route>

            <Route path = "/">
              <Header/>
              <Home/>
            </Route>

          </Switch>
        
      </div>

    </Router>
  );
}

export default App;

/**
 
function App() {
  return (
    <div className="App">
      
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;

 */
