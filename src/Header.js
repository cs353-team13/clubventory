//rfce

import { Link } from 'react-router-dom';
import React from 'react';
import './Header.css';
import { auth } from "./firebase";
function Header() {

    const handleAuthentication = () => {
        if(auth) {
            auth.signOut();
        }
    }

    return (
        
        <div className = 'header'>
           <div className = "rocket__div">
               
               <h3> {auth.currentUser? 'Click on the rocket to logOut?' : 'Click on the rocket to login:..'}</h3>

                
               <Link to = {auth && "/login"}>
                    <div className = "rocket__link" onClick = {handleAuthentication} >🚀</div>
               </Link>
           </div>
           <h3 id = "user__log"> {auth.currentUser? 'Hello ' + auth.currentUser.email : 'Hello there XD why dont you login?'} </h3>
        </div>
    )
}

export default Header
