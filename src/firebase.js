

  // Import the functions you need from the SDKs you need
///import * as firebase from 'firebase';
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth } from "firebase/auth";
import { getFirestore } from 'firebase/firestore';

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyBLCcr9hF1QPK6regEs4Kjy-mos_SWLM_M",
    authDomain: "cs353-team13-6e70d.firebaseapp.com",
    databaseURL: "https://cs353-team13-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "cs353-team13",
    storageBucket: "cs353-team13.appspot.com",
    messagingSenderId: "534665805702",
    appId: "1:534665805702:web:c8daeb305fc635c37ff607",
    measurementId: "G-2ZEQ18PBQ9"
  };

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

const db = getFirestore();
const auth = getAuth();



export { db, auth };
